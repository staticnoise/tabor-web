import styles from "./LinkButton.module.css";
import { Link } from "react-router-dom";

interface LinkButtonInterface {
  text: string;
  link: string;
  style?: React.CSSProperties;
}

const LinkButton = ({ text, link, style }: LinkButtonInterface) => {
  return (
    <div className={styles.link}>
      <Link style={style} to={link}>{text}</Link>
    </div>
  );
};

export default LinkButton;
