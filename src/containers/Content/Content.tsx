import styles from "./Content.module.css";
import { useQuery, gql } from '@apollo/client';
import RichText from "../../lib/renderRichText";

interface InvitationBlockInterface {
  url: string;
}

const InvitationBlock = ({ url }: InvitationBlockInterface) => {

  const { data } = useQuery<{
    landingPageCollection:  {
      items: {
        title: string;
        content: {
          json: any;
          links: {
            assets: {
              block: {
                sys: {
                  id: string
                },
                url: string
              }[]
            };
            entries: {
              block: {
                sys: {
                  id: string;
                }
                title: string;
                nhledovObrzek: {
                  url: string;
                }
              }
            }
          }
        }
      }[]
    }
  }>(gql`{
    landingPageCollection(limit: 1, where: {url: "${url}"}) {
      items {
        title
        content {
          json
          links {
            entries {
              block {
                sys {
                  id
                }
                ... on LandingPage {
                  title
                  url
                  nhledovObrzek {
                    url
                  }
                }
              }
            }
            assets {
              block {
                sys {
                  id
                }
                url
                title
                width
                height
                description
              }
            }
          }
        }
      }
    }
  }`);

  if (!data) {
    return <></>;
  }

  const content = data.landingPageCollection.items[0]?.content;
  const title = data.landingPageCollection.items[0]?.title;

  const alertBanner = <div
      style={{
        backgroundColor: "#ffe6b8",
        padding: "1rem"
      }}
    >
      Naše webové stránky jsou v rekonstrukci, děkujeme za trpělivost. V
      případě dotazů prosím kontaktuje hlavního vedoucího <b>Karla Košnara</b>{" "}
      na emailové adrese <b>karel.kosnar (zavináč) gmail.com</b> nebo na
      telefonu <b>608 906 987</b>.
    </div>

  if (content === undefined || content === null || title === undefined) {
    return (
      <div className={styles.content}>
        {alertBanner}
        <h1>Stránka nenalezena</h1>
      </div>
    )  
  }

  return (
    <>
      <div className={styles.content}>
        {alertBanner}
        <h1>{title}</h1>
        <RichText json={content.json} images={content.links.assets.block} entries={content.links.entries.block} url={url} />
      </div>
    </>
  );
};

export default InvitationBlock;
