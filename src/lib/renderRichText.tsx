import { documentToReactComponents } from "@contentful/rich-text-react-renderer";
import { BLOCKS } from "@contentful/rich-text-types";
import LinkButton from "../components/LinkButton/LinkButton";

interface RichTextInterface {
    json: any;
    url: string;
    images?: any;
    entries?: any;
}

const RichText = ({json, url, images, entries }: RichTextInterface) => {

    const assetMap = new Map();
    if (images !== undefined) {
        for (const asset of images) {
            assetMap.set(asset.sys.id, asset);
        }
    }

    const entryMap = new Map();
    if (entries !== undefined) {
      for (const entry of entries) {
        entryMap.set(entry.sys.id, entry);
      }
    }

    const teamMember = url.startsWith("tym/");
    const teamOverview = url == "tym";

    return documentToReactComponents(json, {
        renderText: (text) => {
          return text.split("\n").reduce((lines, textSegment, index) => {
            return [...lines, index > 0 && <br key={index} />, textSegment];
          }, new Array()); // eslint-disable-line @typescript-eslint/no-array-constructor
        },
        renderNode: {
          [BLOCKS.EMBEDDED_ASSET]: node => {
            const asset = assetMap.get(node.data.target.sys.id);
            return (
              <img
                src={`${asset.url}`}
                alt={asset.title}
                style={{
                    maxWidth: "min(100% - 2rem, 30rem)", margin: "0 auto", width: teamMember ? "17.5rem" : ""
                }}
              />
            );
          },
          [BLOCKS.EMBEDDED_ENTRY]: node => {
            const entry = entryMap.get(node.data.target.sys.id);
            return (
              <div style={{
                width: teamOverview ? "17.5rem" : "", display: teamOverview ? "inline-block" : ""
              }}>
                {entry.nhledovObrzek && <a href={entry.url}><img style={{
                  maxWidth: "100%", maxHeight: "20rem", display: "block"
                }} src={entry.nhledovObrzek.url} /></a> }
                <LinkButton style={{marginTop: "1rem", marginBottom: "1rem", display: "block", textAlign: "center"}} text={entry.title} link={entry.url} />
                
              </div>
            )
          }
        },
      })
}

export default RichText