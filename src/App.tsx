import Content from "./containers/Content/Content";
import MainNav from "./containers/MainNav/MainNav";

import "./App.css";
import { useLocation } from "react-router-dom";
import HomePage from "./containers/HomePage/HomePage";

function App() {

  const location = useLocation();
  const url = location.pathname.slice(1);

  if (url === "") {
    return (
      <>
        <MainNav />
        <HomePage />
      </>
    );
  }

  return (
    <>
      <MainNav />
      <Content url={url} />
    </>
  );
}

export default App;
