import contentful from "contentful";

export type PageSkeleton = {
  contentTypeId: "landing-page";
  fields: {
    title: contentful.EntryFieldTypes.Symbol;
    content: contentful.EntryFieldTypes.RichText;
  };
};

export type PageURLsSkeleton = {
  contentTypeId: "landing-page";
  fields: {
    url: contentful.EntryFieldTypes.Symbol;
  };
};