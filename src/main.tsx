import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.tsx";

import CssBaseline from "@mui/material/CssBaseline";
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";

import { BrowserRouter } from 'react-router-dom';


const apollo_client = new ApolloClient({
  uri: `https://graphql.contentful.com/content/v1/spaces/${import.meta.env.VITE_CONTENTFUL_SPACE_ID}/`,
  cache: new InMemoryCache(),
  headers: {
    Authorization: `Bearer ${import.meta.env.VITE_CONTENTFUL_DELIVERY_TOKEN}`
  },
});

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <CssBaseline />
    <ApolloProvider client={apollo_client}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </ApolloProvider>
  </React.StrictMode>
);
