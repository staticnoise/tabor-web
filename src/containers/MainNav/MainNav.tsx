import LinkButton from "../../components/LinkButton/LinkButton";
import styles from "./MainNav.module.css";
import { Link } from "react-router-dom";

const MainNav = () => {
  return (
    <>
      <div className={styles.MainNav}>
        <div className={styles.title}>
          <Link to="/">Dětský jógový stanový tábor ve Střílkách</Link>
          <p style={{fontSize: "1rem", padding: 0, margin: 0}}>Tradice od roku 2003</p>
        </div>

        <img className={styles.headerImage} src="/tabor.jpg" /> 
      </div>
      <nav className={styles.buttonsWrapper}>
        <LinkButton text="Přihláška" link="/prihlaska" />
        <LinkButton text="Tábor 2023" link="/tajemny-pripad-v-egypte" />
        <LinkButton text="Filozofie" link="/filozofie" />
        <LinkButton text="Tým" link="/tym" />
        <LinkButton text="Minulé tábory" link="/minule-tabory" />
        <LinkButton text="Zpěvník" link="/zpevnik" />
        <LinkButton text="Kontakt" link="/kontakt" />
      </nav>
    </>
);
};

export default MainNav;
