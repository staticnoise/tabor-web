import { gql, useQuery } from "@apollo/client";
import styles from "./HomePage.module.css";
import RichText from "../../lib/renderRichText";

interface HomePageBlockInterface {
  src: string;
  title: string;
  text: any;
  buttonText?: string;
  url: string;
}

const HomePageBlock = ({src, text, title, buttonText, url}: HomePageBlockInterface) => {
  return (
    <div className={styles.row}>
      <div className={styles.innerWrapper}>
        <div className={styles.imgWrapper}>
          <img src={src} className={styles.img} />
        </div>
        <div className={styles.textWrapper}>
          <h2>{title}</h2>
          <RichText json={text} url={url} />
          {buttonText && <a className={styles.textButton} href={url}>{buttonText}</a> }
        </div>
      </div>
    </div>
  )
}

const HomePage = () => {

    const { data } = useQuery<{
        landingPageCollection:  {
          items: {
            title: string;
            poradiNaHlavniStrance: number;
            textTlacitkaSOdkazem: string;
            zobrazitTlacitkoSOdkazem: boolean;
            shrnutiNaHlavniStrance: {
                json: any
            }
            nhledovObrzek: {
                url: string
            }
            url: string;
            content: {
              json: any;
              links: {
                assets: {
                  block: {
                    sys: {
                      id: string
                    },
                    url: string
                  }[]
                }
              }
            }
          }[]
        }
      }>(gql`{
        landingPageCollection(limit: 4, where: {zobrazitNaHlavniStrance: true}) {
          items {
            title
            shrnutiNaHlavniStrance {
                json
            }
            nhledovObrzek {
                url
            }
            poradiNaHlavniStrance
            textTlacitkaSOdkazem
            zobrazitTlacitkoSOdkazem
            url
            content {
              json
              links {
                assets {
                  block {
                    sys {
                      id
                    }
                    url
                    title
                    width
                    height
                    description
                  }
                }
              }
            }
          }
        }
      }`);

    if (!data) {
        return <></>
    }
    
    return (
      <div className={styles.outerWrapper}>
        {data.landingPageCollection.items.map((x) => x).sort((a, b) => a.poradiNaHlavniStrance - b.poradiNaHlavniStrance).map((x) => <HomePageBlock buttonText={x.zobrazitTlacitkoSOdkazem ? x.textTlacitkaSOdkazem : undefined} url={x.url} title={x.title} src={x.nhledovObrzek.url} text={x.shrnutiNaHlavniStrance.json} /> )}
      </div>
    );
}

export default HomePage;